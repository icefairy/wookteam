const mix = require('laravel-mix');
const { VueLoaderPlugin } = require('vue-loader');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


//复制资源
mix.copy('resources/assets/statics/public',     'public');

//生成vue页面js
mix.js('resources/assets/js/main/app.js',       'js').vue({
  version: 2,
});

//生成css样式文件
mix.sass('resources/assets/sass/app.scss',      'css');