# 产品介绍

**[English Documentation](README-EN.md)**

- `wookteam` 是一款轻量级的在线团队协作工具，提供各类文档工具、在线思维导图、在线流程图、项目管理、任务分发，知识库管理等工具。
- `wookteam` 支持团队在线聊天沟通，订阅任务动态实时推送。
- **`wookteam` 全部开源。**

## 安装教程

- [安装教程(Docker)](install/DOCKER.md)
- [安装教程(服务器)](install/SERVER.md)
- [安装教程(宝塔面板)](install/BT.md)

## 官网地址

- [https://www.wookteam.com](https://www.wookteam.com)

## 演示地址

- [https://demo.wookteam.com](https://demo.wookteam.com) (admin/123456)

## 微信咨询

- ![二维码](resources/assets/statics/other/wxqr.jpeg)

## 技术选型

- 后端框架：[Laravel7](https://laravel.com/) + LaravelS
- 前端框架：[Vue 2.0](https://cn.vuejs.org/) + Iview UI
- 数据库：Pgsql、Mysql
- 通讯框架：Swoole
- 主题样式：Kooteam

## 国际化

- `WookTeam`支持国际化，支持：简体中文、英文，英文译文来自谷歌翻译。如果你有更好的语法表达欢迎参与[译文编写](https://docs.google.com/spreadsheets/d/1m0de8-5vCwjKRwW_lsgzsi8wmOmQRl_bIMGN988Keak/edit?usp=sharing)

## 功能简介

**1. 待办四象限：突出事情优先级，帮助员工合理安排时间，提高工作效率**
![待办四象限：突出事情优先级，帮助员工合理安排时间，提高工作效率](resources/assets/statics/images/index/todo.jpg)

**2. 在线流程图：在线流程图工具，使用方便**
![在线流程图：在线流程图工具，使用方便](resources/assets/statics/images/index/banner/1.jpg)

**3. 在线思维导图：梳理思路，优化工作流程**
![在线思维导图：梳理思路，优化工作流程](resources/assets/statics/images/index/banner/2.jpg)

**4. 项目管理：自定义项目看板，可视化任务安排**
![项目管理：自定义项目看板，可视化任务安排](resources/assets/statics/images/index/project.jpg)

**5. 在线知识库：在线流程图，在线文档，以及可视化的目录编排，文档管理无忧**
![在线知识库：在线流程图，在线文档，以及可视化的目录编排，文档管理无忧](resources/assets/statics/images/index/wiki.jpg)

**6. 任务甘特图：可视化任务时间规划，直观方便**
![即时聊天：团队内部沟通，项目动态实时了解](resources/assets/statics/images/index/gantt.jpg)

**7. 即时聊天：团队内部沟通，项目动态实时了解**
![即时聊天：团队内部沟通，项目动态实时了解](resources/assets/statics/images/index/chat.jpg)

## 开发环境说明

### 后端

```bash
composer install
php artisan key:generate
apt install -y inotify-tools fswatch python3 # 更新成python3
php artisan migrate --seed #初始化数据库
cp .env.local .env #本地调试打开LARAVELS_HANDLE_STATIC=true开关，连远程数据库和本地redis
#创建一个数据库迁移任务
php artisan make:migration createproject_step
#编写迁移脚本后执行
php artisan migrate
```

### 前端

```bash
#use nvm 
nvm install 16
nvm use 16
npm config set registry=https://registry.npmmirror.com
npm install -g yarn
yarn config set registry https://registry.npmmirror.com
npm config set sass_binary_site=https://npm.taobao.org/mirrors/node-sass/
npm config set url=http://cnpmjs.org/downloads
npm config set electron_mirror=https://npm.taobao.org/mirrors/electron/
npm config set sqlite3_binary_host_mirror=https://foxgis.oss-cn-shanghai.aliyuncs.com/
npm config set profiler_binary_host_mirror=https://npm.taobao.org/mirrors/node-inspector/
yarn install
yarn dev



```
