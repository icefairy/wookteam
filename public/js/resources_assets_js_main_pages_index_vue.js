(self["webpackChunkwookteam"] = self["webpackChunkwookteam"] || []).push([["resources_assets_js_main_pages_index_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  data: function data() {
    return {
      loadIng: 0,
      loginShow: false,
      loginType: 'login',
      codeNeed: false,
      codeUrl: $A.apiUrl('users/login/codeimg'),
      formLogin: {
        username: '',
        userpass: '',
        userpass2: '',
        code: ''
      },
      ruleLogin: {},
      systemConfig: $A.jsonParse($A.storage("systemSetting")),
      fromUrl: ''
    };
  },
  mounted: function mounted() {//
  },
  activated: function activated() {
    this.getSetting();
    this.fromUrl = decodeURIComponent($A.getObject(this.$route.query, 'from'));

    if (this.fromUrl) {
      this.loginChack();
    }
  },
  deactivated: function deactivated() {
    this.loginShow = false;
  },
  watch: {
    loginShow: function loginShow(val) {
      if (val) {
        this.getSetting();
      } else {
        this.loginType = 'login';
      }
    }
  },
  methods: {
    initLanguage: function initLanguage() {
      var _this = this;

      this.ruleLogin = {
        username: [{
          required: true,
          message: this.$L('请填写用户名！'),
          trigger: 'change'
        }, {
          type: 'string',
          min: 2,
          message: this.$L('用户名长度至少2位！'),
          trigger: 'change'
        }],
        userpass: [{
          required: true,
          message: this.$L('请填写登录密码！'),
          trigger: 'change'
        }, {
          type: 'string',
          min: 6,
          message: this.$L('密码错长度至少6位！'),
          trigger: 'change'
        }],
        userpass2: [{
          required: true,
          message: this.$L('请填写确认密码！'),
          trigger: 'change'
        }, {
          type: 'string',
          min: 6,
          message: this.$L('确认密码错长度至少6位！'),
          trigger: 'change'
        }, {
          validator: function validator(rule, value, callback) {
            if (value !== _this.formLogin.userpass) {
              callback(new Error(_this.$L('两次密码输入不一致！')));
            } else {
              callback();
            }
          },
          required: true,
          trigger: 'change'
        }]
      };
    },
    getSetting: function getSetting() {
      var _this2 = this;

      $A.apiAjax({
        url: 'system/setting',
        error: function error() {
          $A.storage("systemSetting", {});
        },
        success: function success(res) {
          if (res.ret === 1) {
            _this2.systemConfig = res.data;
            $A.storage("systemSetting", _this2.systemConfig);
          } else {
            $A.storage("systemSetting", {});
          }

          if (_this2.systemConfig.loginWin == 'direct') {
            _this2.loginChack();
          }
        }
      });
    },
    loginChack: function loginChack() {
      if ($A.getToken() !== false) {
        this.goForward({
          path: '/todo'
        }, true);
      } else {
        this.loginShow = true;
      }
    },
    refreshCode: function refreshCode() {
      this.codeUrl = $A.apiUrl('users/login/codeimg?_=' + Math.random());
    },
    enterpriseOpen: function enterpriseOpen() {
      this.goForward({
        path: '/plans'
      });
    },
    onBlur: function onBlur() {
      var _this3 = this;

      if (this.loginType != 'login') {
        this.codeNeed = false;
        return;
      }

      this.loadIng++;
      $A.ajax({
        url: $A.apiUrl('users/login/needcode'),
        data: {
          username: this.formLogin.username
        },
        complete: function complete() {
          _this3.loadIng--;
        },
        success: function success(res) {
          _this3.codeNeed = res.ret === 1;
        }
      });
    },
    onLogin: function onLogin() {
      var _this4 = this;

      this.$refs.login.validate(function (valid) {
        if (valid) {
          _this4.loadIng++;
          $A.ajax({
            url: $A.apiUrl('users/login?type=' + _this4.loginType),
            data: _this4.formLogin,
            complete: function complete() {
              _this4.loadIng--;
            },
            success: function success(res) {
              if (res.ret === 1) {
                $A.storage("userInfo", res.data);
                $A.setToken(res.data.token);
                $A.triggerUserInfoListener(res.data); //

                _this4.loadIng--;
                _this4.loginShow = false;

                _this4.$refs.login.resetFields();

                _this4.$Message.success(_this4.$L('登录成功'));

                if (_this4.fromUrl) {
                  window.location.replace(_this4.fromUrl);
                } else {
                  _this4.goForward({
                    path: '/todo'
                  }, true);
                }
              } else {
                _this4.$Modal.error({
                  title: _this4.$L("温馨提示"),
                  content: res.msg
                });

                if (res.data.code === 'need') {
                  _this4.codeNeed = true;

                  _this4.refreshCode();
                }
              }
            }
          });
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=template&id=effe19ba&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=template&id=effe19ba&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "w-box index"
  }, [_c("v-title", [_vm._v(_vm._s(_vm.$L("轻量级的团队在线协作")))]), _vm._v(" "), _vm._m(0), _vm._v(" "), _c("div", {
    staticClass: "header"
  }, [_c("div", {
    staticClass: "z-row"
  }, [_c("div", {
    staticClass: "header-col-sub"
  }, [_c("h2", [_vm.systemConfig.logo ? _c("img", {
    attrs: {
      src: _vm.systemConfig.logo
    }
  }) : _c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/logo-white.png */ "./resources/assets/statics/images/logo-white.png")
    }
  }), _vm._v(" "), _c("span", [_vm._v(_vm._s(_vm.$L("轻量级的团队在线协作")))])])]), _vm._v(" "), _c("div", {
    staticClass: "z-1"
  }, [_c("dl", [_c("dd", [_vm.systemConfig.enterprise == "show" ? _c("Button", {
    staticClass: "right-enterprise",
    attrs: {
      type: "success",
      size: "small"
    },
    on: {
      click: _vm.enterpriseOpen
    }
  }, [_vm._v(_vm._s(_vm.$L("企业版")))]) : _vm._e(), _vm._v(" "), _vm.systemConfig.github == "show" ? _c("a", {
    staticClass: "right-info",
    attrs: {
      href: "https://gitee.com/aipaw/wookteam",
      target: "_blank"
    }
  }, [_c("img", {
    staticClass: "right-img",
    attrs: {
      src: "https://gitee.com/aipaw/wookteam/badge/star.svg?theme=gvp",
      alt: "star"
    }
  })]) : _vm._e(), _vm._v(" "), _vm.systemConfig.github == "show" ? _c("a", {
    staticClass: "right-info",
    attrs: {
      target: "_blank",
      href: "https://gitee.com/aipaw/wookteam"
    }
  }, [_c("Icon", {
    staticClass: "right-icon",
    attrs: {
      type: "logo-github"
    }
  })], 1) : _vm._e(), _vm._v(" "), _c("Dropdown", {
    staticClass: "right-info",
    attrs: {
      trigger: "hover",
      transfer: ""
    },
    on: {
      "on-click": _vm.setLanguage
    }
  }, [_c("div", [_c("Icon", {
    staticClass: "right-icon",
    attrs: {
      type: "md-globe"
    }
  }), _vm._v(" "), _c("Icon", {
    attrs: {
      type: "md-arrow-dropdown"
    }
  })], 1), _vm._v(" "), _c("Dropdown-menu", {
    attrs: {
      slot: "list"
    },
    slot: "list"
  }, [_c("Dropdown-item", {
    attrs: {
      name: "zh",
      selected: _vm.getLanguage() === "zh"
    }
  }, [_vm._v("中文")]), _vm._v(" "), _c("Dropdown-item", {
    attrs: {
      name: "en",
      selected: _vm.getLanguage() === "en"
    }
  }, [_vm._v("English")])], 1)], 1)], 1)])])])]), _vm._v(" "), _c("div", {
    staticClass: "welcome"
  }, [_c("div", {
    staticClass: "banner"
  }, [_c("div", {
    staticClass: "z-row"
  }, [_c("div", {
    staticClass: "z-16"
  }, [_c("Carousel", {
    staticClass: "banner-carousel",
    attrs: {
      autoplay: "",
      loop: "",
      "autoplay-speed": 5000
    }
  }, [_c("CarouselItem", [_c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/index/banner/1.jpg */ "./resources/assets/statics/images/index/banner/1.jpg")
    }
  })]), _vm._v(" "), _c("CarouselItem", [_c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/index/banner/2.jpg */ "./resources/assets/statics/images/index/banner/2.jpg")
    }
  })])], 1)], 1), _vm._v(" "), _c("div", {
    staticClass: "z-8"
  }, [_c("h3", [_vm._v(_vm._s(_vm.$L("酷团队协作工具就从这里开始")))]), _vm._v(" "), _c("div", {
    staticClass: "bl inline-block"
  }, [_c("span", {
    staticClass: "start",
    on: {
      click: _vm.loginChack
    }
  }, [_vm._v(_vm._s(_vm.$L("立即登陆")))])])])])]), _vm._v(" "), _c("div", {
    staticClass: "second"
  }, [_c("div", {
    staticClass: "bg"
  }), _vm._v(" "), _c("div", {
    staticClass: "z-row"
  }, [_c("div", {
    staticClass: "z-6"
  }, [_c("a", {
    attrs: {
      href: "#W_link1"
    }
  }, [_c("i", {
    staticClass: "ft icon"
  }, [_vm._v("")]), _vm._v(_vm._s(_vm.$L("待办四象限")))])]), _vm._v(" "), _c("div", {
    staticClass: "z-6"
  }, [_c("a", {
    attrs: {
      href: "#W_link2"
    }
  }, [_c("i", {
    staticClass: "ft icon"
  }, [_vm._v("")]), _vm._v(_vm._s(_vm.$L("项目管理")))])]), _vm._v(" "), _c("div", {
    staticClass: "z-6"
  }, [_c("a", {
    attrs: {
      href: "#W_link3"
    }
  }, [_c("i", {
    staticClass: "ft icon"
  }, [_vm._v("")]), _vm._v(_vm._s(_vm.$L("在线知识库")))])]), _vm._v(" "), _c("div", {
    staticClass: "z-6"
  }, [_c("a", {
    attrs: {
      href: "#W_link4"
    }
  }, [_c("i", {
    staticClass: "ft icon"
  }, [_vm._v("")]), _vm._v(_vm._s(_vm.$L("日程管理")))])])])])]), _vm._v(" "), _c("div", {
    staticClass: "z-row block"
  }, [_c("div", {
    staticClass: "z-6"
  }, [_c("div", {
    staticClass: "wrap-left",
    attrs: {
      id: "W_link1"
    }
  }, [_c("i", {
    staticClass: "ft icon"
  }, [_vm._v("")]), _vm._v(_vm._s(_vm.$L("待办四象限：突出事情优先级，帮助员工合理安排时间，提高工作效率。")))])]), _vm._v(" "), _vm._m(1)]), _vm._v(" "), _c("div", {
    staticClass: "z-row block"
  }, [_vm._m(2), _vm._v(" "), _c("div", {
    staticClass: "z-6"
  }, [_c("div", {
    staticClass: "wrap-right",
    attrs: {
      id: "W_link2"
    }
  }, [_c("i", {
    staticClass: "ft icon"
  }, [_vm._v("")]), _vm._v(_vm._s(_vm.$L("项目管理：自定义项目看板，可视化任务安排。")))])])]), _vm._v(" "), _c("div", {
    staticClass: "z-row block"
  }, [_c("div", {
    staticClass: "z-6"
  }, [_c("div", {
    staticClass: "wrap-left",
    attrs: {
      id: "W_link3"
    }
  }, [_c("i", {
    staticClass: "ft icon"
  }, [_vm._v("")]), _vm._v(_vm._s(_vm.$L("在线知识库：在线流程图，在线文档，以及可视化的目录编排，文档管理无忧。")))])]), _vm._v(" "), _vm._m(3)]), _vm._v(" "), _c("div", {
    staticClass: "z-row block"
  }, [_vm._m(4), _vm._v(" "), _c("div", {
    staticClass: "z-6"
  }, [_c("div", {
    staticClass: "wrap-right",
    attrs: {
      id: "W_link4"
    }
  }, [_c("i", {
    staticClass: "ft icon"
  }, [_vm._v("")]), _vm._v(_vm._s(_vm.$L("日程管理：可视化日程管理，快速搞定工作计划，了解工作宏观安排。")))])])]), _vm._v(" "), _c("div", {
    staticClass: "p-footer"
  }, [_c("span", {
    domProps: {
      innerHTML: _vm._s(_vm.systemConfig.footerText || "WookTeam &copy; 2018-2020")
    }
  })]), _vm._v(" "), _c("Modal", {
    attrs: {
      "mask-closable": false,
      "class-name": "simple-modal"
    },
    model: {
      value: _vm.loginShow,
      callback: function callback($$v) {
        _vm.loginShow = $$v;
      },
      expression: "loginShow"
    }
  }, [_c("Form", {
    ref: "login",
    attrs: {
      model: _vm.formLogin,
      rules: _vm.ruleLogin
    },
    nativeOn: {
      submit: function submit($event) {
        $event.preventDefault();
      }
    }
  }, [_c("FormItem", {
    attrs: {
      prop: "username"
    }
  }, [_c("Input", {
    attrs: {
      type: "text",
      placeholder: _vm.$L("用户名")
    },
    on: {
      "on-enter": _vm.onLogin,
      "on-blur": _vm.onBlur
    },
    model: {
      value: _vm.formLogin.username,
      callback: function callback($$v) {
        _vm.$set(_vm.formLogin, "username", $$v);
      },
      expression: "formLogin.username"
    }
  }, [_c("Icon", {
    attrs: {
      slot: "prepend",
      type: "ios-person-outline"
    },
    slot: "prepend"
  })], 1)], 1), _vm._v(" "), _c("FormItem", {
    attrs: {
      prop: "userpass"
    }
  }, [_c("Input", {
    attrs: {
      type: "password",
      placeholder: _vm.$L("密码")
    },
    on: {
      "on-enter": _vm.onLogin
    },
    model: {
      value: _vm.formLogin.userpass,
      callback: function callback($$v) {
        _vm.$set(_vm.formLogin, "userpass", $$v);
      },
      expression: "formLogin.userpass"
    }
  }, [_c("Icon", {
    attrs: {
      slot: "prepend",
      type: "ios-lock-outline"
    },
    slot: "prepend"
  })], 1)], 1), _vm._v(" "), _vm.loginType == "reg" ? _c("FormItem", {
    attrs: {
      prop: "userpass2"
    }
  }, [_c("Input", {
    attrs: {
      type: "password",
      placeholder: _vm.$L("确认密码")
    },
    on: {
      "on-enter": _vm.onLogin
    },
    model: {
      value: _vm.formLogin.userpass2,
      callback: function callback($$v) {
        _vm.$set(_vm.formLogin, "userpass2", $$v);
      },
      expression: "formLogin.userpass2"
    }
  }, [_c("Icon", {
    attrs: {
      slot: "prepend",
      type: "ios-lock-outline"
    },
    slot: "prepend"
  })], 1)], 1) : _vm._e(), _vm._v(" "), _vm.loginType == "login" && _vm.codeNeed ? _c("FormItem", {
    attrs: {
      prop: "code"
    }
  }, [_c("Input", {
    attrs: {
      type: "text",
      placeholder: _vm.$L("验证码")
    },
    on: {
      "on-enter": _vm.onLogin
    },
    model: {
      value: _vm.formLogin.code,
      callback: function callback($$v) {
        _vm.$set(_vm.formLogin, "code", $$v);
      },
      expression: "formLogin.code"
    }
  }, [_c("Icon", {
    attrs: {
      slot: "prepend",
      type: "ios-checkmark-circle-outline"
    },
    slot: "prepend"
  }), _vm._v(" "), _c("div", {
    staticClass: "login-code",
    attrs: {
      slot: "append"
    },
    on: {
      click: _vm.refreshCode
    },
    slot: "append"
  }, [_c("img", {
    attrs: {
      src: _vm.codeUrl
    }
  })])], 1)], 1) : _vm._e()], 1), _vm._v(" "), _c("div", {
    staticClass: "login-header",
    attrs: {
      slot: "header"
    },
    slot: "header"
  }, [_c("div", {
    staticClass: "login-header-item",
    "class": {
      active: _vm.loginType == "login"
    },
    on: {
      click: function click($event) {
        _vm.loginType = "login";
      }
    }
  }, [_vm._v(_vm._s(_vm.$L("用户登录")))]), _vm._v(" "), _vm.systemConfig.reg == "open" ? _c("div", {
    staticClass: "login-header-item",
    "class": {
      active: _vm.loginType == "reg"
    },
    on: {
      click: function click($event) {
        _vm.loginType = "reg";
      }
    }
  }, [_vm._v(_vm._s(_vm.$L("注册账号")))]) : _vm._e()]), _vm._v(" "), _c("div", {
    attrs: {
      slot: "footer"
    },
    slot: "footer"
  }, [_c("Button", {
    attrs: {
      type: "default"
    },
    on: {
      click: function click($event) {
        _vm.loginShow = false;
      }
    }
  }, [_vm._v(_vm._s(_vm.$L("取消")))]), _vm._v(" "), _c("Button", {
    attrs: {
      type: "primary",
      loading: _vm.loadIng > 0
    },
    on: {
      click: _vm.onLogin
    }
  }, [_vm._v(_vm._s(_vm.$L(_vm.loginType == "reg" ? "注册" : "登录")))])], 1)], 1)], 1);
};

var staticRenderFns = [function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "welbg"
  }, [_c("div", {
    staticClass: "second"
  }, [_c("div", {
    staticClass: "bg"
  })])]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "z-18"
  }, [_c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/index/todo.jpg */ "./resources/assets/statics/images/index/todo.jpg")
    }
  })]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "z-18"
  }, [_c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/index/project.jpg */ "./resources/assets/statics/images/index/project.jpg")
    }
  })]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "z-18"
  }, [_c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/index/wiki.jpg */ "./resources/assets/statics/images/index/wiki.jpg")
    }
  })]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "z-18"
  }, [_c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/index/week.jpg */ "./resources/assets/statics/images/index/week.jpg")
    }
  })]);
}];
render._withStripped = true;


/***/ }),

/***/ "./resources/assets/statics/images/index/banner/1.jpg":
/*!************************************************************!*\
  !*** ./resources/assets/statics/images/index/banner/1.jpg ***!
  \************************************************************/
/***/ ((module) => {

module.exports = "/images/1.jpg?51317659b15fb56e99bd8396b608cc4b";

/***/ }),

/***/ "./resources/assets/statics/images/index/banner/2.jpg":
/*!************************************************************!*\
  !*** ./resources/assets/statics/images/index/banner/2.jpg ***!
  \************************************************************/
/***/ ((module) => {

module.exports = "/images/2.jpg?e10f7e02852b4d348c0a6c46ac086805";

/***/ }),

/***/ "./resources/assets/statics/images/index/project.jpg":
/*!***********************************************************!*\
  !*** ./resources/assets/statics/images/index/project.jpg ***!
  \***********************************************************/
/***/ ((module) => {

module.exports = "/images/project.jpg?23928737a518ec015d244cab18ec8017";

/***/ }),

/***/ "./resources/assets/statics/images/index/todo.jpg":
/*!********************************************************!*\
  !*** ./resources/assets/statics/images/index/todo.jpg ***!
  \********************************************************/
/***/ ((module) => {

module.exports = "/images/todo.jpg?930c73f91934a7b4b01b031f0fa4c4a3";

/***/ }),

/***/ "./resources/assets/statics/images/index/week.jpg":
/*!********************************************************!*\
  !*** ./resources/assets/statics/images/index/week.jpg ***!
  \********************************************************/
/***/ ((module) => {

module.exports = "/images/week.jpg?bf6fcda5b9f90fa939cc37d2909a95b2";

/***/ }),

/***/ "./resources/assets/statics/images/index/wiki.jpg":
/*!********************************************************!*\
  !*** ./resources/assets/statics/images/index/wiki.jpg ***!
  \********************************************************/
/***/ ((module) => {

module.exports = "/images/wiki.jpg?d618edf4380a9359b8c1caaed07df03e";

/***/ }),

/***/ "./resources/assets/statics/images/logo-white.png":
/*!********************************************************!*\
  !*** ./resources/assets/statics/images/logo-white.png ***!
  \********************************************************/
/***/ ((module) => {

module.exports = "/images/logo-white.png?a742dbdc1be87789abde3f3ca7def987";

/***/ }),

/***/ "./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=style&index=0&id=effe19ba&lang=scss&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=style&index=0&id=effe19ba&lang=scss& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js */ "./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".login-header {\n  display: flex;\n  align-items: center;\n}\n.login-header .login-header-item {\n  height: 20px;\n  line-height: 20px;\n  font-size: 14px;\n  color: #444444;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  padding-right: 12px;\n  cursor: pointer;\n}\n.login-header .login-header-item.active {\n  font-size: 16px;\n  color: #17233d;\n  font-weight: 500;\n}\n.login-code {\n  margin: -4px -7px;\n  height: 30px;\n  overflow: hidden;\n  cursor: pointer;\n}\n.login-code img {\n  height: 100%;\n}", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=style&index=1&id=effe19ba&lang=scss&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=style&index=1&id=effe19ba&lang=scss&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js */ "./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".index[data-v-effe19ba] {\n  position: absolute;\n  color: #000000;\n  top: 0;\n  left: 0;\n  min-width: 100%;\n  min-height: 100%;\n  padding: 0;\n  margin: 0;\n}\n.index .header[data-v-effe19ba] {\n  position: relative;\n  z-index: 3;\n  height: 50px;\n  padding-top: 12px;\n  max-width: 1280px;\n  margin: 0 auto;\n}\n.index .header .z-row[data-v-effe19ba] {\n  color: #fff;\n  height: 50px;\n  position: relative;\n  z-index: 2;\n  max-width: 1680px;\n  margin: 0 auto;\n}\n.index .header .z-row .header-col-sub[data-v-effe19ba] {\n  width: 500px;\n}\n.index .header .z-row .header-col-sub h2[data-v-effe19ba] {\n  position: relative;\n  padding: 1rem 0 0 1rem;\n  display: flex;\n  align-items: flex-end;\n}\n.index .header .z-row .header-col-sub h2 img[data-v-effe19ba] {\n  width: 150px;\n  margin-right: 6px;\n}\n.index .header .z-row .header-col-sub h2 span[data-v-effe19ba] {\n  font-size: 12px;\n  font-weight: normal;\n  color: rgba(255, 255, 255, 0.85);\n  line-height: 14px;\n}\n.index .header .z-row .z-1 dl[data-v-effe19ba] {\n  position: absolute;\n  right: 20px;\n  top: 0;\n  font-size: 14px;\n}\n.index .header .z-row .z-1 dl dd[data-v-effe19ba] {\n  line-height: 50px;\n  color: #fff;\n  cursor: pointer;\n  margin-right: 1px;\n}\n.index .header .z-row .z-1 dl dd .right-enterprise[data-v-effe19ba] {\n  padding: 1px 10px;\n  font-size: 12px;\n  color: #f6ca9d;\n  background: #1d1e23;\n  background: linear-gradient(90deg, #1d1e23, #3f4045);\n  border: none;\n}\n.index .header .z-row .z-1 dl dd .right-info[data-v-effe19ba] {\n  display: inline-block;\n  cursor: pointer;\n  margin-left: 12px;\n  color: #ffffff;\n}\n.index .header .z-row .z-1 dl dd .right-info .right-icon[data-v-effe19ba] {\n  font-size: 26px;\n  vertical-align: middle;\n}\n.index .header .z-row .z-1 dl dd .right-info .right-img[data-v-effe19ba] {\n  vertical-align: middle;\n}\n.index .welbg[data-v-effe19ba] {\n  position: absolute;\n  z-index: 1;\n  top: 0;\n  left: 0;\n  right: 0;\n  height: 762px;\n  overflow: hidden;\n  padding-top: 480px;\n  margin-top: -480px;\n  background: #2d8cf0;\n  transform: skewY(-2deg);\n  box-shadow: 0 2px 244px 0 rgba(56, 132, 255, 0.4);\n}\n.index .welbg .second[data-v-effe19ba] {\n  position: relative;\n  margin-top: 582px;\n  height: 220px;\n}\n.index .welbg .second .bg[data-v-effe19ba] {\n  transform: skewY(-2.5deg);\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: #1F65D6;\n}\n.index .welcome[data-v-effe19ba] {\n  position: relative;\n  z-index: 2;\n  height: 700px;\n  display: block;\n  overflow: hidden;\n  color: #FFFFFF;\n  padding-top: 480px;\n  margin-top: -480px;\n}\n.index .welcome .unslider-arrow[data-v-effe19ba] {\n  display: none;\n}\n.index .welcome .banner[data-v-effe19ba] {\n  padding-top: 60px;\n  height: 460px;\n  max-width: 1200px;\n  margin: 0 auto;\n}\n.index .welcome .banner .banner-carousel[data-v-effe19ba] {\n  max-width: 685px;\n  border-radius: 5px;\n  overflow: hidden;\n}\n.index .welcome .banner img[data-v-effe19ba] {\n  height: 400px;\n  border: 5px solid #fff;\n  display: table;\n}\n.index .welcome .z-8[data-v-effe19ba] {\n  text-align: center;\n}\n.index .welcome h3[data-v-effe19ba] {\n  color: rgba(255, 255, 255, 0.8);\n  font-size: 40px;\n  font-weight: normal;\n  text-align: center;\n  margin: 30px auto;\n  width: 380px;\n}\n.index .welcome .start[data-v-effe19ba] {\n  display: inline-block;\n  width: 160px;\n  height: 50px;\n  line-height: 50px;\n  text-align: center;\n  font-weight: normal;\n  cursor: pointer;\n  font-size: 20px;\n  background: #fff;\n  color: #0396f2;\n  border-radius: 4px;\n  border: 0;\n}\n.index .welcome .start[data-v-effe19ba]:hover, .index .welcome .start[data-v-effe19ba]:focus {\n  background: #f6f6f6;\n  color: #0396f2;\n}\n.index .welcome .second[data-v-effe19ba] {\n  position: relative;\n  height: 220px;\n  text-align: center;\n}\n.index .welcome .second .bg[data-v-effe19ba] {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n}\n.index .welcome .second .z-row[data-v-effe19ba] {\n  z-index: 2;\n  position: relative;\n  font-size: 22px;\n  max-width: 1400px;\n  margin: 0 auto;\n  line-height: 220px;\n}\n.index .welcome .second i[data-v-effe19ba] {\n  color: rgba(255, 255, 255, 0.85);\n  margin-right: 6px;\n}\n.index .welcome .second a[data-v-effe19ba] {\n  color: #fff;\n}\n.index .welcome .second a[data-v-effe19ba]:hover, .index .welcome .second a[data-v-effe19ba]:visited {\n  color: #fff;\n}\n.index .block[data-v-effe19ba] {\n  max-width: 1200px;\n  margin: 30px auto;\n  padding-top: 50px;\n  border: 1px solid transparent;\n}\n.index .block .wrap-left[data-v-effe19ba], .index .block .wrap-right[data-v-effe19ba] {\n  line-height: 36px;\n  color: #666;\n  font-size: 16px;\n}\n.index .block .wrap-left[data-v-effe19ba] {\n  margin: 20px 30px 0 0;\n}\n.index .block .wrap-right[data-v-effe19ba] {\n  margin: 20px 0 0 30px;\n}\n.index .block i[data-v-effe19ba] {\n  color: rgba(248, 14, 21, 0.7);\n  margin-right: 6px;\n}\n.index .block img[data-v-effe19ba] {\n  border: 5px solid #fff;\n  border-radius: 10px;\n  width: 100%;\n}\n.index .p-footer[data-v-effe19ba] {\n  margin: 20px 0;\n  text-align: center;\n  color: #333;\n}\n.index .p-footer a[data-v-effe19ba], .index .p-footer span[data-v-effe19ba] {\n  color: #333;\n  margin-left: 10px;\n}\n@media (max-width: 768px) {\n.index .header .z-row .header-col-sub h2[data-v-effe19ba] {\n    padding: 12px 0 0 12px;\n}\n.index .header .z-row .header-col-sub h2 span[data-v-effe19ba] {\n    display: none;\n}\n.index .welbg[data-v-effe19ba] {\n    display: none;\n}\n.index .welcome[data-v-effe19ba] {\n    height: auto;\n    background: #2d8cf0;\n    transform: skewY(-2deg);\n    box-shadow: 0 2px 244px 0 rgba(56, 132, 255, 0.4);\n}\n.index .welcome .banner[data-v-effe19ba] {\n    height: auto;\n    transform: skewY(2deg);\n}\n.index .welcome .banner .z-row[data-v-effe19ba] {\n    flex-direction: column;\n    padding-bottom: 52px;\n}\n.index .welcome .banner .z-row > div[data-v-effe19ba] {\n    width: 90%;\n    margin: 0 auto;\n}\n.index .welcome .banner h3[data-v-effe19ba] {\n    font-size: 24px;\n    width: auto;\n}\n.index .welcome .banner img[data-v-effe19ba] {\n    max-width: 100%;\n    height: auto;\n}\n.index .welcome .second[data-v-effe19ba] {\n    height: 120px;\n    display: flex;\n    align-items: center;\n    transform: skewY(2deg);\n}\n.index .welcome .second .bg[data-v-effe19ba] {\n    transform: skewY(-4.5deg);\n    background: #1F65D6;\n}\n.index .welcome .second .z-row[data-v-effe19ba] {\n    height: auto;\n    line-height: 36px;\n    font-size: 16px;\n    display: block;\n}\n.index .welcome .second .z-row .z-6[data-v-effe19ba] {\n    width: 30%;\n    margin: 0 5%;\n    white-space: nowrap;\n}\n.index .block[data-v-effe19ba] {\n    flex-direction: column;\n    margin: 24px auto;\n    padding-top: 24px;\n    max-width: 90%;\n    border: 0;\n}\n.index .block > div[data-v-effe19ba] {\n    width: 96%;\n    margin: 0 auto;\n}\n.index .block .wrap-left[data-v-effe19ba],\n.index .block .wrap-right[data-v-effe19ba] {\n    margin: 6px 0;\n    line-height: 28px;\n}\n}", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=style&index=0&id=effe19ba&lang=scss&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=style&index=0&id=effe19ba&lang=scss& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_effe19ba_lang_scss___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!../../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&id=effe19ba&lang=scss& */ "./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=style&index=0&id=effe19ba&lang=scss&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_effe19ba_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_effe19ba_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=style&index=1&id=effe19ba&lang=scss&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=style&index=1&id=effe19ba&lang=scss&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_1_id_effe19ba_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!../../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=1&id=effe19ba&lang=scss&scoped=true& */ "./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=style&index=1&id=effe19ba&lang=scss&scoped=true&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_1_id_effe19ba_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_1_id_effe19ba_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),

/***/ "./resources/assets/js/main/pages/index.vue":
/*!**************************************************!*\
  !*** ./resources/assets/js/main/pages/index.vue ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_vue_vue_type_template_id_effe19ba_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=effe19ba&scoped=true& */ "./resources/assets/js/main/pages/index.vue?vue&type=template&id=effe19ba&scoped=true&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/assets/js/main/pages/index.vue?vue&type=script&lang=js&");
/* harmony import */ var _index_vue_vue_type_style_index_0_id_effe19ba_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&id=effe19ba&lang=scss& */ "./resources/assets/js/main/pages/index.vue?vue&type=style&index=0&id=effe19ba&lang=scss&");
/* harmony import */ var _index_vue_vue_type_style_index_1_id_effe19ba_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=1&id=effe19ba&lang=scss&scoped=true& */ "./resources/assets/js/main/pages/index.vue?vue&type=style&index=1&id=effe19ba&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;



/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_effe19ba_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _index_vue_vue_type_template_id_effe19ba_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "effe19ba",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/main/pages/index.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/assets/js/main/pages/index.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/assets/js/main/pages/index.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/main/pages/index.vue?vue&type=template&id=effe19ba&scoped=true&":
/*!*********************************************************************************************!*\
  !*** ./resources/assets/js/main/pages/index.vue?vue&type=template&id=effe19ba&scoped=true& ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_effe19ba_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_effe19ba_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_effe19ba_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=template&id=effe19ba&scoped=true& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=template&id=effe19ba&scoped=true&");


/***/ }),

/***/ "./resources/assets/js/main/pages/index.vue?vue&type=style&index=0&id=effe19ba&lang=scss&":
/*!************************************************************************************************!*\
  !*** ./resources/assets/js/main/pages/index.vue?vue&type=style&index=0&id=effe19ba&lang=scss& ***!
  \************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_effe19ba_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader/dist/cjs.js!../../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!../../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&id=effe19ba&lang=scss& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=style&index=0&id=effe19ba&lang=scss&");


/***/ }),

/***/ "./resources/assets/js/main/pages/index.vue?vue&type=style&index=1&id=effe19ba&lang=scss&scoped=true&":
/*!************************************************************************************************************!*\
  !*** ./resources/assets/js/main/pages/index.vue?vue&type=style&index=1&id=effe19ba&lang=scss&scoped=true& ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_1_id_effe19ba_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader/dist/cjs.js!../../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!../../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=1&id=effe19ba&lang=scss&scoped=true& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/index.vue?vue&type=style&index=1&id=effe19ba&lang=scss&scoped=true&");


/***/ })

}]);