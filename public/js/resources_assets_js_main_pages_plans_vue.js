(self["webpackChunkwookteam"] = self["webpackChunkwookteam"] || []).push([["resources_assets_js_main_pages_plans_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  data: function data() {
    return {
      active: 2,
      body1: true,
      body2: true,
      contactShow: false,
      systemConfig: $A.jsonParse($A.storage("systemSetting"))
    };
  },
  created: function created() {
    this.addLanguageData({
      "en": {
        "选择合适你的 WookTeam": "You choose the right WookTeam",
        "选择适合您的团队、企业的版本": "Choose your team, the enterprise version",
        "WookTeam 是新一代企业协作平台，您可以根据您企业的业务需求，选择合适的产品功能。": "WookTeam is a new generation of enterprise collaboration platform, you can according to your business needs, choose the right product features.",
        "从现在开始，WookTeam 为世界各地的团队提供支持，探索适合您的选项。": "From now on, WookTeam to support teams around the world, to explore the option for you.",
        "价格": "Price",
        "概述": "Outline",
        "人数": "Number of people",
        "社区版": "Community Edition",
        "团队版": "Team Edition",
        "适用于轻团队的任务协作": "Suitable for light task team collaboration",
        "无限制": "Unlimited",
        "账号：%、密码：%": "Account password:%",
        "体验DEMO": "DEMO",
        "企业版": "Enterprise Edition",
        "推荐": "Recommend",
        "适用于群组共享和高级权限": "Suitable for group sharing and advanced permissions",
        "定制版": "Custom Edition",
        "自定义": "Customize",
        "根据您的需求量身定制": "Tailored to your needs",
        "联系我们": "Contact us",
        "应用支持": "Application Support",
        "文档/知识库": "Document / Knowledge",
        "团队管理": "Team Management",
        "聊天": "To chat with",
        "国际化": "Globalization",
        "任务动态": "Dynamic task",
        "日程": "Agenda",
        "IM群聊": "IM Group Chat",
        "项目群聊": "Project group chat",
        "项目权限": "Project Permissions",
        "项目搜索": "Project Search",
        "任务类型": "Task Type",
        "知识库搜索": "Knowledge Base Search",
        "团队分组": "Team group",
        "分组权限": "Rights group",
        "成员统计": "Member Statistics",
        "签到功能": "Check-ins",
        "服务支持": "Service support",
        "自助支持": "Self-Support",
        "（Issues/文档/社群）": "(Issues / Document / Community)",
        "支持私有化部署": "Support the deployment of privatization",
        "绑定自有域名": "Binding own domain name",
        "二次开发": "Secondary development",
        "在线咨询支持": "Online consulting support",
        "电话咨询支持": "Telephone support",
        "中英文邮件支持": "Mail support in English",
        "一对一客户顾问": "One on one customer service.",
        "产品培训": "Product Training",
        "上门支持": "On-site support",
        "专属客户成功经理": "Dedicated customer success manager",
        "免费提供一次企业内训": "Corporate Training offers a free",
        "明星客户案例": "Star Customer Case",
        "多种部署方式随心选择": "A variety of deployment options to chose freely",
        "公有云": "Public cloud",
        "无需本地环境准备，按需购买帐户，专业团队提供运维保障服务，两周一次的版本迭代": "No local environment preparation, purchase account demand, professional team to provide operation and maintenance support services, bi-weekly version of the iteration",
        "私有云": "Private Cloud",
        "企业隔离的云服务器环境，高可用性，网络及应用层完整隔离，数据高度自主可控": "Enterprise cloud isolated server environments, high availability, network isolation and complete application layer, data is highly self-control",
        "本地服务器": "Local Server",
        "基于 Docker 的容器化部署，支持高可用集群，快速弹性扩展，数据高度自主可控": "Docker container-based deployment, support for high-availability clustering, rapid elasticity expanded data is highly self-control",
        "完善的服务支持体系": "Perfect service support system",
        "1:1客户成功顾问": "1: 1 Customer Success Consultant",
        "资深客户成功顾问对企业进行调研、沟通需求、制定个性化的解决方案，帮助企业落地": "Senior adviser to the success of enterprise customers to conduct research, communicate needs, develop customized solutions that help companies landing",
        "完善的培训体系": "A comprehensive training system",
        "根据需求定制培训内容，为不同角色给出专属培训方案，线上线下培训渠道全覆盖": "According to customized training needs, given exclusive training programs for different roles, training full coverage online and offline channels",
        "全面的支持服务": "Comprehensive support services",
        "多种支持服务让企业无后顾之忧，7*24 线上支持、在线工单、中英文邮件支持、上门支持": "A variety of support services allow enterprises worry-free, 7 * 24 online support, online ticket, in English and e-mail support, on-site support",
        "多重安全策略保护数据": "Multiple security policies to protect data",
        "高可用性保证": "High availability guarantee",
        "多重方式保证数据不丢失，高可用故障转移，异地容灾备份，99.99%可用性保证": "Multiple ways to ensure that data is not lost, high availability failover, offsite disaster recovery, 99.99% availability guarantee",
        "数据加密": "Data encryption",
        "多重方式保证数据不泄漏，基于 TLS 的数据加密传输，DDOS 防御和入侵检测": "Multiple manner to ensure data does not leak, based on TLS encrypted transmission data, intrusion detection and prevention DDOS",
        "帐户安全": "Account Security",
        "多重方式保证帐户安全，远程会话控制，设备绑定，安全日志以及手势密码": "Multiple ways to ensure account security, remote control session, binding equipment, security logs and gesture password",
        "如有任何问题，欢迎使用微信与我们联系。": "If you have any questions, please contact us using the micro-channel."
      }
    });
  },
  mounted: function mounted() {
    this.getSetting();
  },
  methods: {
    getSetting: function getSetting() {
      var _this = this;

      $A.apiAjax({
        url: 'system/setting',
        error: function error() {
          $A.storage("systemSetting", {});
        },
        success: function success(res) {
          if (res.ret === 1) {
            _this.systemConfig = res.data;
            $A.storage("systemSetting", _this.systemConfig);
          } else {
            $A.storage("systemSetting", {});
          }
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=template&id=368018bb&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=template&id=368018bb&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "page-plans"
  }, [_c("v-title", [_vm._v(_vm._s(_vm.$L("选择合适你的 WookTeam")))]), _vm._v(" "), _c("div", {
    staticClass: "top-bg"
  }), _vm._v(" "), _c("div", {
    staticClass: "top-menu"
  }, [_c("div", {
    staticClass: "header"
  }, [_c("div", {
    staticClass: "z-row"
  }, [_c("div", {
    staticClass: "header-col-sub"
  }, [_c("h2", {
    on: {
      click: function click($event) {
        return _vm.goForward({
          path: "/"
        });
      }
    }
  }, [_vm.systemConfig.logo ? _c("img", {
    attrs: {
      src: _vm.systemConfig.logo
    }
  }) : _c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/logo-white.png */ "./resources/assets/statics/images/logo-white.png")
    }
  }), _vm._v(" "), _c("span", [_vm._v(_vm._s(_vm.$L("轻量级的团队在线协作")))])])]), _vm._v(" "), _c("div", {
    staticClass: "z-1"
  }, [_c("dl", [_c("dd", [_vm.systemConfig.github == "show" ? _c("a", {
    staticClass: "right-info",
    attrs: {
      href: "https://gitee.com/aipaw/wookteam",
      target: "_blank"
    }
  }, [_c("img", {
    staticClass: "right-img",
    attrs: {
      src: "https://gitee.com/aipaw/wookteam/badge/star.svg?theme=gvp",
      alt: "star"
    }
  })]) : _vm._e(), _vm._v(" "), _vm.systemConfig.github == "show" ? _c("a", {
    staticClass: "right-info",
    attrs: {
      target: "_blank",
      href: "https://gitee.com/aipaw/wookteam"
    }
  }, [_c("Icon", {
    staticClass: "right-icon",
    attrs: {
      type: "logo-github"
    }
  })], 1) : _vm._e(), _vm._v(" "), _c("Dropdown", {
    staticClass: "right-info",
    attrs: {
      trigger: "hover",
      transfer: ""
    },
    on: {
      "on-click": _vm.setLanguage
    }
  }, [_c("div", [_c("Icon", {
    staticClass: "right-icon",
    attrs: {
      type: "md-globe"
    }
  }), _vm._v(" "), _c("Icon", {
    attrs: {
      type: "md-arrow-dropdown"
    }
  })], 1), _vm._v(" "), _c("Dropdown-menu", {
    attrs: {
      slot: "list"
    },
    slot: "list"
  }, [_c("Dropdown-item", {
    attrs: {
      name: "zh",
      selected: _vm.getLanguage() === "zh"
    }
  }, [_vm._v("中文")]), _vm._v(" "), _c("Dropdown-item", {
    attrs: {
      name: "en",
      selected: _vm.getLanguage() === "en"
    }
  }, [_vm._v("English")])], 1)], 1)], 1)])])])])]), _vm._v(" "), _c("div", {
    staticClass: "banner"
  }, [_c("div", {
    staticClass: "banner-title"
  }, [_vm._v("\n            " + _vm._s(_vm.$L("选择适合您的团队、企业的版本")) + "\n        ")]), _vm._v(" "), _c("div", {
    staticClass: "banner-desc"
  }, [_vm._v("\n            " + _vm._s(_vm.$L("WookTeam 是新一代企业协作平台，您可以根据您企业的业务需求，选择合适的产品功能。")) + " "), _c("br"), _vm._v("\n            " + _vm._s(_vm.$L("从现在开始，WookTeam 为世界各地的团队提供支持，探索适合您的选项。")) + "\n        ")]), _vm._v(" "), _c("div", {
    staticClass: "plans-table"
  }, [_c("div", {
    staticClass: "plans-table-bd plans-table-info"
  }, [_c("div", {
    staticClass: "plans-table-item first"
  }, [_c("div", {
    staticClass: "plans-table-info-th"
  }), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-price"
  }, [_c("em", [_vm._v(_vm._s(_vm.$L("价格")))])]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-desc"
  }, [_c("em", [_vm._v(_vm._s(_vm.$L("概述")))])]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-desc"
  }, [_c("em", [_vm._v(_vm._s(_vm.$L("人数")))])]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-btn"
  })]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-item",
    "class": {
      active: _vm.active == 1
    },
    on: {
      mouseenter: function mouseenter($event) {
        _vm.active = 1;
      }
    }
  }, [_c("div", {
    staticClass: "plans-table-info-th"
  }, [_vm._v(_vm._s(_vm.$L("团队版")))]), _vm._v(" "), _vm._m(0), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-desc"
  }, [_vm._v(_vm._s(_vm.$L("适用于轻团队的任务协作")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-desc"
  }, [_vm._v(_vm._s(_vm.$L("无限制")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-btn"
  }, [_c("div", {
    staticClass: "plans-info-btns"
  }, [_c("Tooltip", {
    attrs: {
      content: _vm.$L("账号：%、密码：%", "admin", "123456"),
      transfer: ""
    }
  }, [_c("a", {
    staticClass: "btn",
    attrs: {
      href: "https://demo.wookteam.com",
      target: "_blank"
    }
  }, [_vm._v(_vm._s(_vm.$L("体验DEMO")))])]), _vm._v(" "), _c("a", {
    staticClass: "github",
    attrs: {
      href: "https://gitee.com/aipaw/wookteam",
      target: "_blank"
    }
  }, [_c("Icon", {
    attrs: {
      type: "logo-github"
    }
  })], 1)], 1)])]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-item",
    "class": {
      active: _vm.active == 2
    },
    on: {
      mouseenter: function mouseenter($event) {
        _vm.active = 2;
      }
    }
  }, [_c("div", {
    staticClass: "plans-table-info-th"
  }, [_vm._v(_vm._s(_vm.$L("企业版")) + " "), _c("span", [_vm._v(_vm._s(_vm.$L("推荐")))])]), _vm._v(" "), _vm._m(1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-desc"
  }, [_vm._v(_vm._s(_vm.$L("适用于群组共享和高级权限")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-desc"
  }, [_vm._v(_vm._s(_vm.$L("无限制")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-btn"
  }, [_c("Tooltip", {
    attrs: {
      content: _vm.$L("账号：%、密码：%", "admin", "123456"),
      transfer: ""
    }
  }, [_c("a", {
    staticClass: "btn",
    attrs: {
      href: "https://pro.wookteam.com",
      target: "_blank"
    }
  }, [_vm._v(_vm._s(_vm.$L("体验DEMO")))])])], 1)]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-item",
    "class": {
      active: _vm.active == 3
    },
    on: {
      mouseenter: function mouseenter($event) {
        _vm.active = 3;
      }
    }
  }, [_c("div", {
    staticClass: "plans-table-info-th"
  }, [_vm._v(_vm._s(_vm.$L("定制版")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-price"
  }, [_c("img", {
    staticClass: "plans-version",
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/plans/Ultimate.png */ "./resources/assets/statics/images/plans/Ultimate.png")
    }
  }), _vm._v(" "), _c("div", {
    staticClass: "currency"
  }, [_c("em", {
    staticClass: "custom"
  }, [_vm._v(_vm._s(_vm.$L("自定义")))])])]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-desc"
  }, [_vm._v(_vm._s(_vm.$L("根据您的需求量身定制")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-desc"
  }, [_vm._v(_vm._s(_vm.$L("无限制")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-btn"
  }, [_c("a", {
    staticClass: "btn btn-contact",
    attrs: {
      href: "javascript:void(0)"
    },
    on: {
      click: function click($event) {
        _vm.contactShow = true;
      }
    }
  }, [_vm._v(_vm._s(_vm.$L("联系我们")))])])])]), _vm._v(" "), _c("div", {
    staticClass: "plans-accordion-head",
    "class": {
      "plans-accordion-close": !_vm.body1
    },
    on: {
      click: function click($event) {
        _vm.body1 = !_vm.body1;
      }
    }
  }, [_c("div", {
    staticClass: "first"
  }, [_c("span", [_vm._v(_vm._s(_vm.$L("应用支持")))])]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-item",
    "class": {
      active: _vm.active == 1
    },
    on: {
      mouseenter: function mouseenter($event) {
        _vm.active = 1;
      }
    }
  }), _vm._v(" "), _c("div", {
    staticClass: "plans-table-item",
    "class": {
      active: _vm.active == 2
    },
    on: {
      mouseenter: function mouseenter($event) {
        _vm.active = 2;
      }
    }
  }), _vm._v(" "), _c("div", {
    staticClass: "plans-table-item",
    "class": {
      active: _vm.active == 3
    },
    on: {
      mouseenter: function mouseenter($event) {
        _vm.active = 3;
      }
    }
  }), _vm._v(" "), _c("span", [_c("Icon", {
    attrs: {
      type: "ios-arrow-down"
    }
  })], 1)]), _vm._v(" "), _vm.body1 ? _c("div", {
    staticClass: "plans-accordion-body"
  }, [_c("div", {
    staticClass: "plans-table-bd plans-table-app"
  }, [_c("div", {
    staticClass: "plans-table-item first"
  }, [_c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("项目管理")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("文档/知识库")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("团队管理")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v("IM" + _vm._s(_vm.$L("聊天")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("子任务")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("国际化")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("甘特图")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("任务动态")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("导出任务")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("日程")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("周报/日报")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("IM群聊")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("项目群聊")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("项目权限")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("项目搜索")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("任务类型")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("知识库搜索")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("团队分组")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("分组权限")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("成员统计")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("签到功能")))])]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-item",
    "class": {
      active: _vm.active == 1
    },
    on: {
      mouseenter: function mouseenter($event) {
        _vm.active = 1;
      }
    }
  }, [_c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(" - ")]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(" - ")]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(" - ")]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(" - ")]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(" - ")]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(" - ")]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(" - ")]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(" - ")]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(" - ")]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(" - ")])]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-item",
    "class": {
      active: _vm.active == 2
    },
    on: {
      mouseenter: function mouseenter($event) {
        _vm.active = 2;
      }
    }
  }, [_c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1)]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-item",
    "class": {
      active: _vm.active == 3
    },
    on: {
      mouseenter: function mouseenter($event) {
        _vm.active = 3;
      }
    }
  }, [_c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1)])])]) : _vm._e(), _vm._v(" "), _c("div", {
    staticClass: "plans-accordion-head",
    "class": {
      "plans-accordion-close": !_vm.body2
    },
    on: {
      click: function click($event) {
        _vm.body2 = !_vm.body2;
      }
    }
  }, [_c("div", {
    staticClass: "first"
  }, [_c("span", [_vm._v(_vm._s(_vm.$L("服务支持")))])]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-item",
    "class": {
      active: _vm.active == 1
    },
    on: {
      mouseenter: function mouseenter($event) {
        _vm.active = 1;
      }
    }
  }), _vm._v(" "), _c("div", {
    staticClass: "plans-table-item",
    "class": {
      active: _vm.active == 2
    },
    on: {
      mouseenter: function mouseenter($event) {
        _vm.active = 2;
      }
    }
  }), _vm._v(" "), _c("div", {
    staticClass: "plans-table-item",
    "class": {
      active: _vm.active == 3
    },
    on: {
      mouseenter: function mouseenter($event) {
        _vm.active = 3;
      }
    }
  }), _vm._v(" "), _c("span", [_c("Icon", {
    attrs: {
      type: "ios-arrow-down"
    }
  })], 1)]), _vm._v(" "), _vm.body2 ? _c("div", {
    staticClass: "plans-accordion-body"
  }, [_c("div", {
    staticClass: "plans-table-bd plans-table-app plans-table-service"
  }, [_c("div", {
    staticClass: "plans-table-item first"
  }, [_c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("自助支持")) + " "), _c("span", [_vm._v(_vm._s(_vm.$L("（Issues/文档/社群）")))])]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("支持私有化部署")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("绑定自有域名")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("二次开发")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("在线咨询支持")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("电话咨询支持")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("中英文邮件支持")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("一对一客户顾问")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("产品培训")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("上门支持")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("专属客户成功经理")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("免费提供一次企业内训")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_vm._v(_vm._s(_vm.$L("明星客户案例")))]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-btn"
  })]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-item",
    "class": {
      active: _vm.active == 1
    },
    on: {
      mouseenter: function mouseenter($event) {
        _vm.active = 1;
      }
    }
  }, [_c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _vm._m(2), _vm._v(" "), _vm._m(3), _vm._v(" "), _vm._m(4), _vm._v(" "), _vm._m(5), _vm._v(" "), _vm._m(6), _vm._v(" "), _vm._m(7), _vm._v(" "), _vm._m(8), _vm._v(" "), _vm._m(9), _vm._v(" "), _vm._m(10), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-btn"
  }, [_c("div", {
    staticClass: "plans-info-btns"
  }, [_c("Tooltip", {
    attrs: {
      content: _vm.$L("账号：%、密码：%", "admin", "123456"),
      transfer: ""
    }
  }, [_c("a", {
    staticClass: "btn",
    attrs: {
      href: "https://demo.wookteam.com",
      target: "_blank"
    }
  }, [_vm._v(_vm._s(_vm.$L("体验DEMO")))])]), _vm._v(" "), _c("a", {
    staticClass: "github",
    attrs: {
      href: "https://gitee.com/aipaw/wookteam",
      target: "_blank"
    }
  }, [_c("Icon", {
    attrs: {
      type: "logo-github"
    }
  })], 1)], 1)])]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-item",
    "class": {
      active: _vm.active == 2
    },
    on: {
      mouseenter: function mouseenter($event) {
        _vm.active = 2;
      }
    }
  }, [_c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _vm._m(11), _vm._v(" "), _vm._m(12), _vm._v(" "), _vm._m(13), _vm._v(" "), _vm._m(14), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-btn"
  }, [_c("Tooltip", {
    attrs: {
      content: _vm.$L("账号：%、密码：%", "admin", "123456"),
      transfer: ""
    }
  }, [_c("a", {
    staticClass: "btn",
    attrs: {
      href: "https://pro.wookteam.com",
      target: "_blank"
    }
  }, [_vm._v(_vm._s(_vm.$L("体验DEMO")))])])], 1)]), _vm._v(" "), _c("div", {
    staticClass: "plans-table-item",
    "class": {
      active: _vm.active == 3
    },
    on: {
      mouseenter: function mouseenter($event) {
        _vm.active = 3;
      }
    }
  }, [_c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-td"
  }, [_c("Icon", {
    attrs: {
      type: "md-checkmark"
    }
  })], 1), _vm._v(" "), _c("div", {
    staticClass: "plans-table-info-btn"
  }, [_c("a", {
    staticClass: "btn btn-contact",
    attrs: {
      href: "javascript:void(0)"
    },
    on: {
      click: function click($event) {
        _vm.contactShow = true;
      }
    }
  }, [_vm._v(_vm._s(_vm.$L("联系我们")))])])])])]) : _vm._e()])]), _vm._v(" "), _c("div", {
    staticClass: "container-fluid"
  }, [_c("div", {
    staticClass: "fluid-info fluid-info-1"
  }, [_c("div", {
    staticClass: "fluid-info-item"
  }, [_c("div", {
    staticClass: "info-title"
  }, [_vm._v("\n                    " + _vm._s(_vm.$L("多种部署方式随心选择")) + "\n                ")]), _vm._v(" "), _c("div", {
    staticClass: "info-function"
  }, [_c("div", {
    staticClass: "func-item"
  }, [_vm._m(15), _vm._v(" "), _c("div", {
    staticClass: "func-desc"
  }, [_c("div", {
    staticClass: "desc-title"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("公有云")) + "\n                            ")]), _vm._v(" "), _c("div", {
    staticClass: "desc-text"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("无需本地环境准备，按需购买帐户，专业团队提供运维保障服务，两周一次的版本迭代")) + "\n                            ")])])]), _vm._v(" "), _c("div", {
    staticClass: "func-item"
  }, [_vm._m(16), _vm._v(" "), _c("div", {
    staticClass: "func-desc"
  }, [_c("div", {
    staticClass: "desc-title"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("私有云")) + "\n                            ")]), _vm._v(" "), _c("div", {
    staticClass: "desc-text"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("企业隔离的云服务器环境，高可用性，网络及应用层完整隔离，数据高度自主可控")) + "\n                            ")])])]), _vm._v(" "), _c("div", {
    staticClass: "func-item"
  }, [_vm._m(17), _vm._v(" "), _c("div", {
    staticClass: "func-desc"
  }, [_c("div", {
    staticClass: "desc-title"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("本地服务器")) + "\n                            ")]), _vm._v(" "), _c("div", {
    staticClass: "desc-text"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("基于 Docker 的容器化部署，支持高可用集群，快速弹性扩展，数据高度自主可控")) + "\n                            ")])])])])])]), _vm._v(" "), _c("div", {
    staticClass: "fluid-info"
  }, [_c("div", {
    staticClass: "fluid-info-item"
  }, [_c("div", {
    staticClass: "info-title"
  }, [_vm._v("\n                    " + _vm._s(_vm.$L("完善的服务支持体系")) + "\n                ")]), _vm._v(" "), _c("div", {
    staticClass: "info-function"
  }, [_c("div", {
    staticClass: "func-item"
  }, [_vm._m(18), _vm._v(" "), _c("div", {
    staticClass: "func-desc"
  }, [_c("div", {
    staticClass: "desc-title"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("1:1客户成功顾问")) + "\n                            ")]), _vm._v(" "), _c("div", {
    staticClass: "desc-text"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("资深客户成功顾问对企业进行调研、沟通需求、制定个性化的解决方案，帮助企业落地")) + "\n                            ")])])]), _vm._v(" "), _c("div", {
    staticClass: "func-item"
  }, [_vm._m(19), _vm._v(" "), _c("div", {
    staticClass: "func-desc"
  }, [_c("div", {
    staticClass: "desc-title"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("完善的培训体系")) + "\n                            ")]), _vm._v(" "), _c("div", {
    staticClass: "desc-text"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("根据需求定制培训内容，为不同角色给出专属培训方案，线上线下培训渠道全覆盖")) + "\n                            ")])])]), _vm._v(" "), _c("div", {
    staticClass: "func-item"
  }, [_vm._m(20), _vm._v(" "), _c("div", {
    staticClass: "func-desc"
  }, [_c("div", {
    staticClass: "desc-title"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("全面的支持服务")) + "\n                            ")]), _vm._v(" "), _c("div", {
    staticClass: "desc-text"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("多种支持服务让企业无后顾之忧，7*24 线上支持、在线工单、中英文邮件支持、上门支持")) + "\n                            ")])])])])])]), _vm._v(" "), _c("div", {
    staticClass: "fluid-info fluid-info-3"
  }, [_c("div", {
    staticClass: "fluid-info-item"
  }, [_c("div", {
    staticClass: "info-title"
  }, [_vm._v("\n                    " + _vm._s(_vm.$L("多重安全策略保护数据")) + "\n                ")]), _vm._v(" "), _c("div", {
    staticClass: "info-function"
  }, [_c("div", {
    staticClass: "func-item"
  }, [_vm._m(21), _vm._v(" "), _c("div", {
    staticClass: "func-desc"
  }, [_c("div", {
    staticClass: "desc-title"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("高可用性保证")) + "\n                            ")]), _vm._v(" "), _c("div", {
    staticClass: "desc-text"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("多重方式保证数据不丢失，高可用故障转移，异地容灾备份，99.99%可用性保证")) + "\n                            ")])])]), _vm._v(" "), _c("div", {
    staticClass: "func-item"
  }, [_vm._m(22), _vm._v(" "), _c("div", {
    staticClass: "func-desc"
  }, [_c("div", {
    staticClass: "desc-title"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("数据加密")) + "\n                            ")]), _vm._v(" "), _c("div", {
    staticClass: "desc-text"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("多重方式保证数据不泄漏，基于 TLS 的数据加密传输，DDOS 防御和入侵检测")) + "\n                            ")])])]), _vm._v(" "), _c("div", {
    staticClass: "func-item"
  }, [_vm._m(23), _vm._v(" "), _c("div", {
    staticClass: "func-desc"
  }, [_c("div", {
    staticClass: "desc-title"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("帐户安全")) + "\n                            ")]), _vm._v(" "), _c("div", {
    staticClass: "desc-text"
  }, [_vm._v("\n                                " + _vm._s(_vm.$L("多重方式保证帐户安全，远程会话控制，设备绑定，安全日志以及手势密码")) + "\n                            ")])])])])])])]), _vm._v(" "), _vm._m(24), _vm._v(" "), _c("Modal", {
    attrs: {
      title: _vm.$L("联系我们"),
      "class-name": "simple-modal",
      width: "430",
      "footer-hide": ""
    },
    model: {
      value: _vm.contactShow,
      callback: function callback($$v) {
        _vm.contactShow = $$v;
      },
      expression: "contactShow"
    }
  }, [_c("div", {
    staticClass: "contact-modal"
  }, [_c("p", [_vm._v(_vm._s(_vm.$L("如有任何问题，欢迎使用微信与我们联系。")))]), _vm._v(" "), _c("p", [_c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/plans/wechat.png */ "./resources/assets/statics/images/plans/wechat.png")
    }
  })])])])], 1);
};

var staticRenderFns = [function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "plans-table-info-price"
  }, [_c("img", {
    staticClass: "plans-version",
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/plans/free.png */ "./resources/assets/statics/images/plans/free.png")
    }
  }), _vm._v(" "), _c("div", {
    staticClass: "currency"
  }, [_c("em", [_vm._v("0")])])]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "plans-table-info-price"
  }, [_c("img", {
    staticClass: "plans-version",
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/plans/pro.png */ "./resources/assets/statics/images/plans/pro.png")
    }
  }), _vm._v(" "), _c("div", {
    staticClass: "currency"
  }, [_c("em", [_vm._v("18800")])])]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "plans-table-td"
  }, [_c("span", [_vm._v(" - ")])]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "plans-table-td"
  }, [_c("span", [_vm._v(" - ")])]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "plans-table-td"
  }, [_c("span", [_vm._v(" - ")])]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "plans-table-td"
  }, [_c("span", [_vm._v(" - ")])]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "plans-table-td"
  }, [_c("span", [_vm._v(" - ")])]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "plans-table-td"
  }, [_c("span", [_vm._v(" - ")])]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "plans-table-td"
  }, [_c("span", [_vm._v(" - ")])]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "plans-table-td"
  }, [_c("span", [_vm._v(" - ")])]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "plans-table-td"
  }, [_c("span", [_vm._v(" - ")])]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "plans-table-td"
  }, [_c("span", [_vm._v(" - ")])]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "plans-table-td"
  }, [_c("span", [_vm._v(" - ")])]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "plans-table-td"
  }, [_c("span", [_vm._v(" - ")])]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "plans-table-td"
  }, [_c("span", [_vm._v(" - ")])]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "image"
  }, [_c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/plans/1.svg */ "./resources/assets/statics/images/plans/1.svg")
    }
  })]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "image"
  }, [_c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/plans/2.svg */ "./resources/assets/statics/images/plans/2.svg")
    }
  })]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "image image-80"
  }, [_c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/plans/3.svg */ "./resources/assets/statics/images/plans/3.svg")
    }
  })]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "image"
  }, [_c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/plans/4.svg */ "./resources/assets/statics/images/plans/4.svg")
    }
  })]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "image image-80"
  }, [_c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/plans/5.svg */ "./resources/assets/statics/images/plans/5.svg")
    }
  })]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "image"
  }, [_c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/plans/6.svg */ "./resources/assets/statics/images/plans/6.svg")
    }
  })]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "image"
  }, [_c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/plans/7.svg */ "./resources/assets/statics/images/plans/7.svg")
    }
  })]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "image image-80"
  }, [_c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/plans/8.svg */ "./resources/assets/statics/images/plans/8.svg")
    }
  })]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "image image-50"
  }, [_c("img", {
    attrs: {
      src: __webpack_require__(/*! ../../../statics/images/plans/9.svg */ "./resources/assets/statics/images/plans/9.svg")
    }
  })]);
}, function () {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "contact-footer"
  }, [_c("span", [_vm._v("WookTeam © 2018-2020")])]);
}];
render._withStripped = true;


/***/ }),

/***/ "./resources/assets/statics/images/logo-white.png":
/*!********************************************************!*\
  !*** ./resources/assets/statics/images/logo-white.png ***!
  \********************************************************/
/***/ ((module) => {

module.exports = "/images/logo-white.png?a742dbdc1be87789abde3f3ca7def987";

/***/ }),

/***/ "./resources/assets/statics/images/plans/1.svg":
/*!*****************************************************!*\
  !*** ./resources/assets/statics/images/plans/1.svg ***!
  \*****************************************************/
/***/ ((module) => {

module.exports = "/images/1.svg?dff373bedfaed448fb7d0179e5022397";

/***/ }),

/***/ "./resources/assets/statics/images/plans/2.svg":
/*!*****************************************************!*\
  !*** ./resources/assets/statics/images/plans/2.svg ***!
  \*****************************************************/
/***/ ((module) => {

module.exports = "/images/2.svg?7bc8d80f1f9e4b6be39aaf15824bd659";

/***/ }),

/***/ "./resources/assets/statics/images/plans/3.svg":
/*!*****************************************************!*\
  !*** ./resources/assets/statics/images/plans/3.svg ***!
  \*****************************************************/
/***/ ((module) => {

module.exports = "/images/3.svg?ae295c9bfa4c1e558c7d9039a3b4f99c";

/***/ }),

/***/ "./resources/assets/statics/images/plans/4.svg":
/*!*****************************************************!*\
  !*** ./resources/assets/statics/images/plans/4.svg ***!
  \*****************************************************/
/***/ ((module) => {

module.exports = "/images/4.svg?62bb721c8435fb4586514ac72ce15a87";

/***/ }),

/***/ "./resources/assets/statics/images/plans/5.svg":
/*!*****************************************************!*\
  !*** ./resources/assets/statics/images/plans/5.svg ***!
  \*****************************************************/
/***/ ((module) => {

module.exports = "/images/5.svg?4c1f6f31b662cd0bfa61c45d2d0dfef3";

/***/ }),

/***/ "./resources/assets/statics/images/plans/6.svg":
/*!*****************************************************!*\
  !*** ./resources/assets/statics/images/plans/6.svg ***!
  \*****************************************************/
/***/ ((module) => {

module.exports = "/images/6.svg?2e8c823eac32776194c4cbdd3341a2f9";

/***/ }),

/***/ "./resources/assets/statics/images/plans/7.svg":
/*!*****************************************************!*\
  !*** ./resources/assets/statics/images/plans/7.svg ***!
  \*****************************************************/
/***/ ((module) => {

module.exports = "/images/7.svg?7498af428dd5a24d2f80f7fd552726a6";

/***/ }),

/***/ "./resources/assets/statics/images/plans/8.svg":
/*!*****************************************************!*\
  !*** ./resources/assets/statics/images/plans/8.svg ***!
  \*****************************************************/
/***/ ((module) => {

module.exports = "/images/8.svg?2e12c62cc8e600aed842f80e2d8ffb7a";

/***/ }),

/***/ "./resources/assets/statics/images/plans/9.svg":
/*!*****************************************************!*\
  !*** ./resources/assets/statics/images/plans/9.svg ***!
  \*****************************************************/
/***/ ((module) => {

module.exports = "/images/9.svg?110f5e782aaa315f8020c4f74b69cd74";

/***/ }),

/***/ "./resources/assets/statics/images/plans/Ultimate.png":
/*!************************************************************!*\
  !*** ./resources/assets/statics/images/plans/Ultimate.png ***!
  \************************************************************/
/***/ ((module) => {

module.exports = "/images/Ultimate.png?b2ab20239e8a160c7490f0eb7c407b7e";

/***/ }),

/***/ "./resources/assets/statics/images/plans/banner-bg.png":
/*!*************************************************************!*\
  !*** ./resources/assets/statics/images/plans/banner-bg.png ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = "/images/banner-bg.png?7e9381790d1d55bbe24a6463ba1e9999";

/***/ }),

/***/ "./resources/assets/statics/images/plans/bg_04.jpg":
/*!*********************************************************!*\
  !*** ./resources/assets/statics/images/plans/bg_04.jpg ***!
  \*********************************************************/
/***/ ((module) => {

module.exports = "/images/bg_04.jpg?88126c4e375e60c4bc2e7f7b078f184c";

/***/ }),

/***/ "./resources/assets/statics/images/plans/free.png":
/*!********************************************************!*\
  !*** ./resources/assets/statics/images/plans/free.png ***!
  \********************************************************/
/***/ ((module) => {

module.exports = "/images/free.png?3f17bfc759ae4ba88b45bee959997fc9";

/***/ }),

/***/ "./resources/assets/statics/images/plans/pro.png":
/*!*******************************************************!*\
  !*** ./resources/assets/statics/images/plans/pro.png ***!
  \*******************************************************/
/***/ ((module) => {

module.exports = "/images/pro.png?e5991da67f0dfbeaaf5765f48a975326";

/***/ }),

/***/ "./resources/assets/statics/images/plans/wechat.png":
/*!**********************************************************!*\
  !*** ./resources/assets/statics/images/plans/wechat.png ***!
  \**********************************************************/
/***/ ((module) => {

module.exports = "/images/wechat.png?386c7530433268dc4e4a583536134ffc";

/***/ }),

/***/ "./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=style&index=0&id=368018bb&lang=scss&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=style&index=0&id=368018bb&lang=scss&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js */ "./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".contact-modal p[data-v-368018bb] {\n  padding: 0;\n  margin: 0;\n  font-size: 16px;\n  text-align: center;\n}\n.contact-modal p img[data-v-368018bb] {\n  display: inline-block;\n  width: 248px;\n}", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=style&index=1&id=368018bb&lang=scss&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=style&index=1&id=368018bb&lang=scss&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js */ "./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../node_modules/laravel-mix/node_modules/css-loader/dist/runtime/getUrl.js */ "./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/getUrl.js");
/* harmony import */ var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _statics_images_plans_banner_bg_png__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../statics/images/plans/banner-bg.png */ "./resources/assets/statics/images/plans/banner-bg.png");
/* harmony import */ var _statics_images_plans_banner_bg_png__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_statics_images_plans_banner_bg_png__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _statics_images_plans_bg_04_jpg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../statics/images/plans/bg_04.jpg */ "./resources/assets/statics/images/plans/bg_04.jpg");
/* harmony import */ var _statics_images_plans_bg_04_jpg__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_statics_images_plans_bg_04_jpg__WEBPACK_IMPORTED_MODULE_3__);
// Imports




var ___CSS_LOADER_EXPORT___ = _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
var ___CSS_LOADER_URL_REPLACEMENT_0___ = _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()((_statics_images_plans_banner_bg_png__WEBPACK_IMPORTED_MODULE_2___default()));
var ___CSS_LOADER_URL_REPLACEMENT_1___ = _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()((_statics_images_plans_bg_04_jpg__WEBPACK_IMPORTED_MODULE_3___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, "@charset \"UTF-8\";\n.page-plans .top-bg[data-v-368018bb] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 640px;\n  padding-top: 192px;\n  z-index: 0;\n  background: url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") center top no-repeat;\n  background-size: 100% 100%;\n}\n.page-plans .top-menu[data-v-368018bb] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  z-index: 2;\n}\n.page-plans .top-menu .header[data-v-368018bb] {\n  height: 50px;\n  padding-top: 12px;\n  max-width: 1280px;\n  margin: 0 auto;\n}\n.page-plans .top-menu .header .z-row[data-v-368018bb] {\n  color: #fff;\n  height: 50px;\n  position: relative;\n  z-index: 2;\n  max-width: 1680px;\n  margin: 0 auto;\n}\n.page-plans .top-menu .header .z-row .header-col-sub[data-v-368018bb] {\n  width: 500px;\n}\n.page-plans .top-menu .header .z-row .header-col-sub h2[data-v-368018bb] {\n  position: relative;\n  padding: 1rem 0 0 1rem;\n  display: flex;\n  align-items: flex-end;\n  cursor: pointer;\n}\n.page-plans .top-menu .header .z-row .header-col-sub h2 img[data-v-368018bb] {\n  width: 150px;\n  margin-right: 6px;\n}\n.page-plans .top-menu .header .z-row .header-col-sub h2 span[data-v-368018bb] {\n  font-size: 12px;\n  font-weight: normal;\n  color: #ffffff;\n  line-height: 14px;\n}\n.page-plans .top-menu .header .z-row .z-1 dl[data-v-368018bb] {\n  position: absolute;\n  right: 20px;\n  top: 0;\n  font-size: 14px;\n}\n.page-plans .top-menu .header .z-row .z-1 dl dd[data-v-368018bb] {\n  line-height: 50px;\n  color: #fff;\n  cursor: pointer;\n  margin-right: 1px;\n}\n.page-plans .top-menu .header .z-row .z-1 dl dd .right-info[data-v-368018bb] {\n  display: inline-block;\n  cursor: pointer;\n  margin-left: 12px;\n  color: #ffffff;\n}\n.page-plans .top-menu .header .z-row .z-1 dl dd .right-info .right-icon[data-v-368018bb] {\n  font-size: 26px;\n  vertical-align: middle;\n}\n.page-plans .top-menu .header .z-row .z-1 dl dd .right-info .right-img[data-v-368018bb] {\n  vertical-align: middle;\n}\n.page-plans .banner[data-v-368018bb] {\n  position: relative;\n  z-index: 1;\n  padding-top: 192px;\n}\n.page-plans .banner .banner-title[data-v-368018bb] {\n  font-size: 50px;\n  text-align: center;\n  padding: 0 10px;\n  color: #fff;\n}\n.page-plans .banner .banner-desc[data-v-368018bb] {\n  font-size: 14px;\n  color: #fff;\n  text-align: center;\n  padding: 0 25px;\n  max-width: 940px;\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: 40px;\n  line-height: 30px;\n}\n.page-plans .banner .plans-table[data-v-368018bb] {\n  max-width: 1120px;\n  margin: 110px auto 100px;\n  box-shadow: 0 10px 30px rgba(172, 184, 207, 0.3);\n}\n.page-plans .banner .plans-table em[data-v-368018bb] {\n  font-style: normal;\n  font-size: 14px;\n  color: #666666;\n}\n.page-plans .banner .plans-table .plans-table-bd[data-v-368018bb] {\n  background-color: #fff;\n  display: flex;\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item[data-v-368018bb] {\n  flex: 1;\n  border-left: 1px solid #eee;\n  position: relative;\n  z-index: 1;\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item > div[data-v-368018bb] {\n  transition: background 0.3s;\n  border-bottom: 1px solid #eee;\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item > div[data-v-368018bb]:first-child, .page-plans .banner .plans-table .plans-table-bd .plans-table-item > div[data-v-368018bb]:last-child {\n  border-bottom: none;\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item[data-v-368018bb]:first-child {\n  flex: none;\n  width: 27.7%;\n  border-left: none;\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item[data-v-368018bb]::before {\n  content: \"\";\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  top: 0;\n  background: transparent;\n  border-radius: 0;\n  z-index: -2;\n  transform: scaleY(1);\n  transition: all 0.3s;\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item.active[data-v-368018bb] {\n  position: relative;\n  border-left-color: transparent;\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item.active > div[data-v-368018bb] {\n  border-color: transparent !important;\n  background: transparent;\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item.active[data-v-368018bb]::before {\n  z-index: -1;\n  border-radius: 2px;\n  background: #fff;\n  transform: scaleY(1.05);\n  box-shadow: 0 10px 30px rgba(172, 184, 207, 0.3);\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item.active + .plans-table-item[data-v-368018bb] {\n  border-left-color: transparent;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-td[data-v-368018bb] {\n  height: 60px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-td[data-v-368018bb]:first-child {\n  border-bottom: 1px solid #eee !important;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-td > span[data-v-368018bb] {\n  font-family: -apple-system, Arial, sans-serif;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item .plans-table-td[data-v-368018bb] {\n  position: relative;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item .plans-table-td i[data-v-368018bb] {\n  color: #22d7bb;\n  font-size: 20px;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item .plans-table-td > .info[data-v-368018bb] {\n  position: absolute;\n  font-size: 12px;\n  color: #888;\n  top: 50%;\n  left: 50%;\n  transform: translate(30%, -50%);\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item .plans-table-info-btn[data-v-368018bb] {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  height: 100px;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td[data-v-368018bb] {\n  font-size: 14px;\n  color: #666;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td i[data-v-368018bb] {\n  width: 34px;\n  font-size: 20px;\n  text-align: center;\n  transform: translateX(-5px);\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td:nth-child(1) i[data-v-368018bb] {\n  color: #ff7747;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td:nth-child(2) i[data-v-368018bb] {\n  color: #f669a7;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td:nth-child(3) i[data-v-368018bb] {\n  color: #ffa415;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td:nth-child(4) i[data-v-368018bb] {\n  color: #2dbcff;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td:nth-child(5) i[data-v-368018bb] {\n  color: #66c060;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td:nth-child(6) i[data-v-368018bb] {\n  color: #99d75a;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td:nth-child(7) i[data-v-368018bb] {\n  color: #4e8af9;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td:nth-child(8) i[data-v-368018bb] {\n  color: #ff5b57;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td.plans-table-app-okr[data-v-368018bb] {\n  position: relative;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td.plans-table-app-okr[data-v-368018bb]::after {\n  content: \"(OKR)\";\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(90%, -50%);\n}\n.page-plans .banner .plans-table .plans-table-info-flex[data-v-368018bb] {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-th[data-v-368018bb] {\n  height: 70px;\n  background-color: #eef2f8;\n  font-size: 16px;\n  color: #485778;\n  line-height: 70px;\n  text-align: center;\n  font-weight: 600;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-th span[data-v-368018bb] {\n  height: 18px;\n  line-height: 18px;\n  font-size: 14px;\n  padding: 0 8px;\n  background-color: #fa3d3f;\n  border-radius: 2px;\n  color: #fff;\n  font-weight: normal;\n  margin-left: 7px;\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-price[data-v-368018bb] {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  height: 265px;\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-price .plans-version[data-v-368018bb] {\n  margin-bottom: 30px;\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-price .currency[data-v-368018bb] {\n  height: 35px;\n  position: relative;\n  margin-bottom: 18px;\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-price .currency[data-v-368018bb]::before {\n  content: \"￥\";\n  color: #485778;\n  position: absolute;\n  font-size: 18px;\n  left: 0;\n  top: 0;\n  transform: translate(-110%, -20%);\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-price .currency > em[data-v-368018bb] {\n  font-size: 36px;\n  font-weight: 900;\n  display: inline-block;\n  margin-top: -10px;\n  height: 56px;\n  line-height: 56px;\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-price .currency > em.custom[data-v-368018bb] {\n  font-size: 24px;\n  font-weight: 500;\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-desc[data-v-368018bb] {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  height: 70px;\n  font-size: 14px;\n  color: #aaaaaa;\n}\n.page-plans .banner .plans-table .plans-table-info-btn[data-v-368018bb] {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  height: 115px;\n}\n.page-plans .banner .plans-table .plans-table-info-btn .plans-info-btns[data-v-368018bb] {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n}\n.page-plans .banner .plans-table .plans-table-info-btn .plans-info-btns .btn[data-v-368018bb] {\n  padding: 14px 36px;\n}\n.page-plans .banner .plans-table .plans-table-info-btn .plans-info-btns .github[data-v-368018bb] {\n  margin-left: 10px;\n}\n.page-plans .banner .plans-table .plans-table-info-btn .plans-info-btns .github > i[data-v-368018bb] {\n  font-size: 32px;\n}\n.page-plans .banner .plans-table .plans-table-info-btn .btn[data-v-368018bb] {\n  display: inline-block;\n  color: #fff;\n  background-color: #348FE4;\n  border-color: #348FE4;\n  padding: 14px 54px;\n  font-size: 14px;\n  line-height: 14px;\n  border-radius: 30px;\n  outline: none;\n}\n.page-plans .banner .plans-table .plans-table-info-btn .btn.btn-contact[data-v-368018bb] {\n  background-color: #6BC853;\n  border-color: #6BC853;\n}\n.page-plans .banner .plans-table .plans-accordion-head[data-v-368018bb] {\n  height: 60px;\n  line-height: 60px;\n  background-color: #eef2f8;\n  position: relative;\n  z-index: 2;\n  display: flex;\n  cursor: pointer;\n}\n.page-plans .banner .plans-table .plans-accordion-head > div[data-v-368018bb] {\n  width: 27.7%;\n  flex: 1;\n}\n.page-plans .banner .plans-table .plans-accordion-head > div.first[data-v-368018bb] {\n  width: 27.7%;\n  flex: none;\n}\n.page-plans .banner .plans-table .plans-accordion-head > div.first > span[data-v-368018bb] {\n  font-weight: 600;\n  color: #333333;\n  font-size: 14px;\n  padding-left: 30px;\n}\n.page-plans .banner .plans-table .plans-accordion-head > span[data-v-368018bb] {\n  position: absolute;\n  top: 0;\n  right: 30px;\n  line-height: 60px;\n  height: 60px;\n  transition: transform 0.3s;\n}\n.page-plans .banner .plans-table .plans-accordion-head > span i[data-v-368018bb] {\n  font-size: 20px;\n  color: #aaa;\n}\n.page-plans .banner .plans-table .plans-accordion-head.plans-accordion-close > span[data-v-368018bb] {\n  transform: rotate(90deg);\n}\n.page-plans .container-fluid[data-v-368018bb] {\n  margin-left: auto;\n  margin-right: auto;\n}\n.page-plans .container-fluid .fluid-info.fluid-info-1[data-v-368018bb] {\n  border-bottom: 1px solid #dddddd;\n}\n.page-plans .container-fluid .fluid-info.fluid-info-3[data-v-368018bb] {\n  background: url(" + ___CSS_LOADER_URL_REPLACEMENT_1___ + ");\n  background-size: 100% 100%;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item[data-v-368018bb] {\n  max-width: 1120px;\n  margin: 0 auto;\n  height: 780px;\n  padding: 130px 0;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item .info-title[data-v-368018bb] {\n  text-align: center;\n  font-size: 42px;\n  color: #333333;\n  margin-bottom: 110px;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item .info-function .func-item[data-v-368018bb] {\n  float: left;\n  width: 33%;\n  text-align: center;\n  padding: 0 40px;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item .info-function .func-item .image[data-v-368018bb] {\n  height: 215px;\n  margin: 0 auto 40px;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item .info-function .func-item .image img[data-v-368018bb] {\n  width: 63%;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item .info-function .func-item .image.image-80 img[data-v-368018bb] {\n  width: 78%;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item .info-function .func-item .image.image-50 img[data-v-368018bb] {\n  width: 50%;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item .info-function .func-item .func-desc .desc-title[data-v-368018bb] {\n  font-size: 16px;\n  color: #333333;\n  margin-bottom: 27px;\n  font-weight: 600;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item .info-function .func-item .func-desc .desc-text[data-v-368018bb] {\n  color: #888888;\n  line-height: 24px;\n}\n.page-plans .contact-footer[data-v-368018bb] {\n  margin: 20px 0;\n  text-align: center;\n  color: #333;\n}\n.page-plans .contact-footer a[data-v-368018bb], .page-plans .contact-footer span[data-v-368018bb] {\n  color: #333;\n  margin-left: 10px;\n}", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/getUrl.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/getUrl.js ***!
  \*********************************************************************************/
/***/ ((module) => {

"use strict";


module.exports = function (url, options) {
  if (!options) {
    // eslint-disable-next-line no-param-reassign
    options = {};
  } // eslint-disable-next-line no-underscore-dangle, no-param-reassign


  url = url && url.__esModule ? url.default : url;

  if (typeof url !== "string") {
    return url;
  } // If url is already wrapped in quotes, remove them


  if (/^['"].*['"]$/.test(url)) {
    // eslint-disable-next-line no-param-reassign
    url = url.slice(1, -1);
  }

  if (options.hash) {
    // eslint-disable-next-line no-param-reassign
    url += options.hash;
  } // Should url be wrapped?
  // See https://drafts.csswg.org/css-values-3/#urls


  if (/["'() \t\n]/.test(url) || options.needQuotes) {
    return "\"".concat(url.replace(/"/g, '\\"').replace(/\n/g, "\\n"), "\"");
  }

  return url;
};

/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=style&index=0&id=368018bb&lang=scss&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=style&index=0&id=368018bb&lang=scss&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_plans_vue_vue_type_style_index_0_id_368018bb_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!../../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./plans.vue?vue&type=style&index=0&id=368018bb&lang=scss&scoped=true& */ "./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=style&index=0&id=368018bb&lang=scss&scoped=true&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_plans_vue_vue_type_style_index_0_id_368018bb_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_plans_vue_vue_type_style_index_0_id_368018bb_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=style&index=1&id=368018bb&lang=scss&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=style&index=1&id=368018bb&lang=scss&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_plans_vue_vue_type_style_index_1_id_368018bb_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!../../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./plans.vue?vue&type=style&index=1&id=368018bb&lang=scss&scoped=true& */ "./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=style&index=1&id=368018bb&lang=scss&scoped=true&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_plans_vue_vue_type_style_index_1_id_368018bb_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_plans_vue_vue_type_style_index_1_id_368018bb_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),

/***/ "./resources/assets/js/main/pages/plans.vue":
/*!**************************************************!*\
  !*** ./resources/assets/js/main/pages/plans.vue ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _plans_vue_vue_type_template_id_368018bb_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./plans.vue?vue&type=template&id=368018bb&scoped=true& */ "./resources/assets/js/main/pages/plans.vue?vue&type=template&id=368018bb&scoped=true&");
/* harmony import */ var _plans_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./plans.vue?vue&type=script&lang=js& */ "./resources/assets/js/main/pages/plans.vue?vue&type=script&lang=js&");
/* harmony import */ var _plans_vue_vue_type_style_index_0_id_368018bb_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./plans.vue?vue&type=style&index=0&id=368018bb&lang=scss&scoped=true& */ "./resources/assets/js/main/pages/plans.vue?vue&type=style&index=0&id=368018bb&lang=scss&scoped=true&");
/* harmony import */ var _plans_vue_vue_type_style_index_1_id_368018bb_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./plans.vue?vue&type=style&index=1&id=368018bb&lang=scss&scoped=true& */ "./resources/assets/js/main/pages/plans.vue?vue&type=style&index=1&id=368018bb&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;



/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__["default"])(
  _plans_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _plans_vue_vue_type_template_id_368018bb_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _plans_vue_vue_type_template_id_368018bb_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "368018bb",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/main/pages/plans.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/assets/js/main/pages/plans.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/assets/js/main/pages/plans.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_plans_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./plans.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_plans_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/main/pages/plans.vue?vue&type=template&id=368018bb&scoped=true&":
/*!*********************************************************************************************!*\
  !*** ./resources/assets/js/main/pages/plans.vue?vue&type=template&id=368018bb&scoped=true& ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_plans_vue_vue_type_template_id_368018bb_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_plans_vue_vue_type_template_id_368018bb_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_plans_vue_vue_type_template_id_368018bb_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./plans.vue?vue&type=template&id=368018bb&scoped=true& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=template&id=368018bb&scoped=true&");


/***/ }),

/***/ "./resources/assets/js/main/pages/plans.vue?vue&type=style&index=0&id=368018bb&lang=scss&scoped=true&":
/*!************************************************************************************************************!*\
  !*** ./resources/assets/js/main/pages/plans.vue?vue&type=style&index=0&id=368018bb&lang=scss&scoped=true& ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_plans_vue_vue_type_style_index_0_id_368018bb_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader/dist/cjs.js!../../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!../../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./plans.vue?vue&type=style&index=0&id=368018bb&lang=scss&scoped=true& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=style&index=0&id=368018bb&lang=scss&scoped=true&");


/***/ }),

/***/ "./resources/assets/js/main/pages/plans.vue?vue&type=style&index=1&id=368018bb&lang=scss&scoped=true&":
/*!************************************************************************************************************!*\
  !*** ./resources/assets/js/main/pages/plans.vue?vue&type=style&index=1&id=368018bb&lang=scss&scoped=true& ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_plans_vue_vue_type_style_index_1_id_368018bb_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader/dist/cjs.js!../../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!../../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./plans.vue?vue&type=style&index=1&id=368018bb&lang=scss&scoped=true& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/assets/js/main/pages/plans.vue?vue&type=style&index=1&id=368018bb&lang=scss&scoped=true&");


/***/ })

}]);