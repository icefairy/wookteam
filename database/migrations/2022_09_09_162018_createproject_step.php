<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateprojectStep extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_step', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('projectid')->nullable()->default(0)->comment('项目ID');
            $table->integer('pid')->default(0)->comment('任务ID');
            $table->text('title')->nullable()->comment('显示名称');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_step');
    }
}
