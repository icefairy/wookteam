<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Addtaskcode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_task', function (Blueprint $table) {
            $table->text('tcode')->after('complete2')->nullable()->default('')->comment('任务编码');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_task', function (Blueprint $table) {
            $table->dropColumn('tcode');
        });
    }
}
