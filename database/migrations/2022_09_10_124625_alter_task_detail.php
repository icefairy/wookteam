<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTaskDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_task', function (Blueprint $table) {
            $table->integer('step_id')->after('next_task')->nullable()->default(0)->comment('栏目(模块)id');
            $table->text('complete2')->after('next_task')->nullable()->default('')->comment('完成方式');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_task', function (Blueprint $table) {
            $table->dropColumn('step_id');
            $table->dropColumn('complete2');
        });
    }
}
