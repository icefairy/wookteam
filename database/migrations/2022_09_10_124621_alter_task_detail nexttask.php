<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTaskDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_task', function (Blueprint $table) {
            $table->text('next_task')->after('indate')->nullable()->default('0')->comment('下一任务');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_task', function (Blueprint $table) {
            $table->dropColumn('next_task');
        });
    }
}
